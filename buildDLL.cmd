@ECHO OFF
IF EXIST .\out (
rmdir /s /q .\out
)
mkdir .\out\dll
g++ -std=c++11 -Wall -O4 -shared -o .\out\dll\BitState.dll .\src\c++\objs\BitState.cpp
g++ -std=c++11 -Wall -O4 -shared -o .\out\dll\AutomatedPlayer.dll .\src\c++\objs\MonteCarlo.cpp .\src\c++\objs\BitState.cpp .\src\c++\objs\AutomatedPlayer.cpp .\src\c++\objs\Strategy.cpp
g++ -std=c++11 -Wall -O4 -shared -o .\out\dll\MonteCarlo.dll .\src\c++\objs\MonteCarlo.cpp .\src\c++\objs\BitState.cpp .\src\c++\objs\AutomatedPlayer.cpp .\src\c++\objs\Strategy.cpp