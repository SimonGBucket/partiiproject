import DLLLoader
import ctypes
import BitState
_monteCarloDLL = DLLLoader.loadDLL("MonteCarlo.dll")

_monteCarloDLL.newMonteCarlo.restype = ctypes.c_void_p
_monteCarloDLL.newMonteCarlo.argtypes = None

_monteCarloDLL.newMonteCarloFromState.restype = ctypes.c_void_p
_monteCarloDLL.newMonteCarloFromState.argtypes = [ctypes.c_void_p]

_monteCarloDLL.deleteMonteCarlo.restype = None
_monteCarloDLL.deleteMonteCarlo.argtypes = [ctypes.c_void_p]

_monteCarloDLL.runCycle.restype = None
_monteCarloDLL.runCycle.argtypes = [ctypes.c_void_p, ctypes.c_uint32]

_monteCarloDLL.getMaxAverageScoreChild.restype = ctypes.c_uint8
_monteCarloDLL.getMaxAverageScoreChild.argtypes = [ctypes.c_void_p]

class MonteCarloWrapper():

    @staticmethod
    def fromState(state : BitState):
        return MonteCarloWrapper(_monteCarloDLL.newMonteCarloFromState(state.getCObj()))

    def __init__(self, cObj = None):
        if (cObj):
            self._cobj = cObj
        else:
            self._cobj = _monteCarloDLL.newMonteCarlo()

    def __exit__(self, exc_type, exc_val, exc_tb):
        _monteCarloDLL.deleteMonteCarlo(self._cobj)

    def runCycle(self, numberOfCyles):
        _monteCarloDLL.runCycle(self._cobj, numberOfCyles)

    def getMaxAverageScoreChild(self):
        return _monteCarloDLL.getMaxAverageScoreChild(self._cobj)