from enum import IntEnum
class Move(IntEnum):
    LEFT = 0
    UP = 1
    RIGHT = 2
    DOWN = 3