from abc import ABCMeta, abstractmethod
from GameState import GameState
from Move import Move
import AutomatedPlayer
from random import randint
class Strategy(metaclass=ABCMeta):
    @abstractmethod
    def getNextMove(gameState):
        pass


class StrategyRandom(Strategy):
    def getNextMove(gameState):
        legalMoves = gameState.getLegalMoves()
        return legalMoves[randint(0, len(legalMoves) - 1)]

class StrategyAverageRandom(Strategy):
    def getNextMove(gameState):
        legalMoves = list(gameState.getLegalMoves())
        maxResult = (0, None)
        for m in legalMoves:
            gameCopy = gameState.copy()
            gameCopy.makeMove(m)
            player = AutomatedPlayer.AutomatedPlayer(gameCopy, StrategyRandom, 100)
            (score, _) = player.play()
            if (score > maxResult[0]):
                maxResult = (score, m)
        return maxResult[1]

class StrategyFewestTiles(Strategy):
    def getNextMove(gameState):
        legalMoves = list(gameState.getLegalMoves())
        tileCountResult = (4 ** 2 + 1, 0, None)
        for m in legalMoves:
            gameCopy = gameState.copy()
            gameCopy.makeMove(m)
            tileCount = gameCopy.getTileCount()
            if (tileCount < tileCountResult[0]
            or tileCount == tileCountResult and gameCopy.score > tileCountResult[1]):
                tileCountResult = (tileCount, gameCopy.getScore(), m)
        return tileCountResult[2]