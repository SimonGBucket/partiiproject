import Tree
import BitState
import Strategy
import AutomatedPlayer
import AutomatedPlayerWrapper
import math
import datetime

class MonteCarloTree(Tree.MutableTree):

    def __init__(self, gameState, parent, moveFromParent, numberOfPlays = 0, totalScore = 0, children = None):
        Tree.MutableTree.__init__(
            self,
            {
                "state": gameState,
                "moveFromParent": moveFromParent,
                "numberOfPlays": numberOfPlays,
                "totalScore": totalScore
            },
            parent,
            children
        )
    
    def getState(self):
        return self.getValue()["state"]

    def getMoveFromParent(self):
        return self.getValue()["moveFromParent"]

    def getNumberOfPlays(self):
        return self.getValue()["numberOfPlays"]

    def updateNumberOfPlays(self, increment):
        self.getValue()["numberOfPlays"] += increment

    def getTotalScore(self):
        return self.getValue()["totalScore"]

    def updateTotalScore(self, increment):
        self.getValue()["totalScore"] += increment


class MonteCarlo():
    """
    Class implementing Monte Carlo Tree Search.
    """
    __SIMULATION_STRATEGY = Strategy.StrategyRandom
    __EXPLORATION_PARAMETER = math.sqrt(2)
    __SIMULATE_TIME = datetime.timedelta(microseconds=10000)
    
    def __init__(self, rootState = None):
        if (not(rootState)):
            rootState = BitState.BitState()
        self.__tree = MonteCarloTree(rootState, None, 0, 0)

    def runCycle(self, numberOfCycles = 1):
        """
        Run a specified number of cycles of MCTS

        Parameters
        ----------
        numberOfCycles: number of cycles to run

        Returns
        -------
        NONE
        """
        for _ in range(numberOfCycles):
            optimalPlayNode = self.__select(self.__tree)
            newLeafs = self.__expand(optimalPlayNode)
            for leaf in newLeafs:
                (gameCount, totalScore) = self.__simulate(leaf)
                self.__backPropagate(leaf, gameCount, totalScore)

    def getOptimalPlayout(self, startNode = None):
        """
        Give the optimal playout according to current MCST.

        Parameters
        ----------
        startNode: node to give optimal playout from 

        Returns
        -------
        out: list of moves resulting in optimal playout
        """
        if (not(startNode)):
            startNode = self.__tree
        
        optimalNode = startNode
        playout = []
        while (not(optimalNode.isLeafNode())):
            optimalNode = max(optimalNode.getChildren(), key=MonteCarlo.__getUCTUpperBound)
            playout.append((optimalNode.getTotalScore(), optimalNode.getNumberOfPlays(), optimalNode.getMoveFromParent()))
        return playout

    def __getUCTUpperBound(treeNode):
        """
        Get the upper bound score of a node based on UCT algorithm 

        Parameters
        ----------
        treeNode: node for which to compute upper bound score 

        Returns
        -------
        out: upper bound score of provided node
        """
        numberOfPlays = treeNode.getNumberOfPlays()
        parentPlays = treeNode.getParent().getNumberOfPlays()
        if (numberOfPlays == 0):
            raise Exception("Monte Carlo Node with 0 plays") # Should never happen
        return treeNode.getTotalScore() / numberOfPlays + MonteCarlo.__EXPLORATION_PARAMETER * math.sqrt(math.log(numberOfPlays) / parentPlays)

    def __select(self, treeNode):
        """
        Selection fase of MCTS. Starts in treeNode.

        Parameters
        ----------
        treeNode: current root of selection stage 

        Returns
        -------
        out: optimal leaf node of path
        """   
        if (treeNode.isLeafNode()):
            return treeNode
        optimalNode = treeNode
        while (not(optimalNode.isLeafNode())):
            optimalNode = max(optimalNode.getChildren(), key=MonteCarlo.__getUCTUpperBound)
        return optimalNode

    def __expand(self, treeNode):
        """
        Expansion fase of MCTS. Create child using a random move.

        Parameters
        ----------
        treeNode: leaf node to expand

        Returns
        -------
        out: list of created leaf nodes in the tree
        """
        legalMoves = treeNode.getState().getLegalMoves()
        newStates = [treeNode.getState().copy() for _ in legalMoves]
        for i in range(len(legalMoves)):
            newStates[i].makeMove(legalMoves[i])

        newNodes = [MonteCarloTree(newStates[i], treeNode, legalMoves[i], 0, 0) for i in range(len(legalMoves))]
        for node in newNodes:
            treeNode.addChild(node)
        return newNodes

    def __simulate(self, treeNode):
        """
        Simulation fase of MCTS.
        Run for self.__simulateTime seconds.
        Run using MonteCarlo.SIMULATION_STRATEGY

        Parameters
        ----------
        treeNode: node to run simulation for 

        Returns
        -------
        (count, score): tuple of number of games played and the total score
        """
        autoPlayer = AutomatedPlayerWrapper.AutomatedPlayerWrapper.RandomPlayer(treeNode.getState(), 1)
        begin = datetime.datetime.utcnow()
        gameCount = 0
        scoreSum = 0
        while datetime.datetime.utcnow() - begin < MonteCarlo.__SIMULATE_TIME:
            gameCount += 1
            autoPlayer.play()
            scoreSum += autoPlayer.getTotalScore()

        return (gameCount, scoreSum)

    def __backPropagate(self, treeNode, gameCount, scoreSum):
        """
        Back propagation fase of MCTS. Recursively update the game values.

        Parameters
        ----------
        treeNode: node to update
        gameCount: number of extra games played
        scoreSum: total score of those extra games

        Returns
        -------
        NONE
        """
        treeNode.updateNumberOfPlays(gameCount)
        treeNode.updateTotalScore(scoreSum)
        
        if (not(treeNode.isRootNode())):
            self.__backPropagate(treeNode.getParent(), gameCount, scoreSum)
        
m = MonteCarlo()
m.runCycle(1000)
print(m.getOptimalPlayout())