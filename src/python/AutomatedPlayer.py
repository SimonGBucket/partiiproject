import Strategy
from GameState import GameState
class AutomatedPlayer():
    def __init__(self, gameState, strategy, numberOfGames = 1):
        self._initialGameState = gameState
        self._strategy = strategy
        self._numberOfGames = numberOfGames
        self._totalScore = 0
        self._totalMoves = 0
    
    def _playGame(self):
        g = self._initialGameState.copy()
        moveCount = 0
        while (not(g.isFinished())):
            m = self._strategy.getNextMove(g)
            g.makeMove(m)
            moveCount += 1
        return (g.getScore(), moveCount)
    
    def play(self):
        self._totalScore = 0
        self._totalMoves = 0
        for i in range(self._numberOfGames):
            (gameScore, gameMoves) = self._playGame()
            self._totalScore += gameScore
            self._totalMoves += gameMoves

    def getTotalScore(self):
        return self._totalScore

    def getNumberOfMoves(self):
        return self._totalMoves
        