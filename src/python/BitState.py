import Move
import DLLLoader
import ctypes

_bitStateDLL = DLLLoader.loadDLL("BitState.dll")

_bitStateDLL.newBitState.restype = ctypes.c_void_p
_bitStateDLL.newBitState.argtypes = [ctypes.c_ubyte, ctypes.c_float]

_bitStateDLL.copyBitState.restype = ctypes.c_void_p
_bitStateDLL.copyBitState.argtypes = [ctypes.c_void_p]

_bitStateDLL.destroyBitState.restype = None
_bitStateDLL.destroyBitState.argtypes = [ctypes.c_void_p]

_bitStateDLL.makeMove.restype = None
_bitStateDLL.makeMove.argtypes = [ctypes.c_void_p, ctypes.c_ubyte]

_bitStateDLL.getLegalMoves.restype = ctypes.c_uint8
_bitStateDLL.getLegalMoves.argtypes = [ctypes.c_void_p]

_bitStateDLL.gameFinished.restype = ctypes.c_bool
_bitStateDLL.gameFinished.argtypes = [ctypes.c_void_p]

_bitStateDLL.getScore.restype = ctypes.c_uint32
_bitStateDLL.getScore.argtypes = [ctypes.c_void_p]

_bitStateDLL.getTileValue.restype = ctypes.c_uint32
_bitStateDLL.getTileValue.argtypes = [ctypes.c_void_p, ctypes.c_ubyte, ctypes.c_ubyte]

_bitStateDLL.getTileCount.restype = ctypes.c_uint32
_bitStateDLL.getTileCount.argtypes = [ctypes.c_void_p]

_bitStateDLL.getUnsignedBinaryState.restype = ctypes.c_uint64
_bitStateDLL.getUnsignedBinaryState.argtypes = [ctypes.c_void_p]
# _bitStateDLL.clearTile.restype = None
# _bitStateDLL.clearTile.argtypes = [ctypes.c_void_p, ctypes.c_ubyte, ctypes.c_ubyte]

# _bitStateDLL.getTile.restype = ctypes.c_ubyte
# _bitStateDLL.getTile.argtypes = [ctypes.c_void_p, ctypes.c_ubyte, ctypes.c_ubyte]

# _bitStateDLL.setTile.restype = None
# _bitStateDLL.setTile.argtypes = [ctypes.c_void_p, ctypes.c_ubyte, ctypes.c_ubyte, ctypes.c_ubyte]

class BitState():

    legalMoveBitPatternMap = [
        [m for m in Move.Move if ((i >> m.value) & 0b1) == 0b1] for i in range(2 ** 4)
    ]
    # Most Significant Bit Mask
    MSBSetMask = 0b1 << 63

    def __init__(self, cobj = None, boardSize = 4, tile2Chance = 0.9):
        if (cobj):
            self._cobj = cobj
        else:
            self._cobj = _bitStateDLL.newBitState(boardSize, tile2Chance)
        self.__legalMoves = None

    def copy(self):
        return BitState(_bitStateDLL.copyBitState(self._cobj))
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        _bitStateDLL.destroyBitState(self._cobj)

    def getCObj(self):
        return self._cobj

    def makeMove(self, move):
        if (isinstance(move, Move.Move)):
            move = move.value
        try:
            _bitStateDLL.makeMove(self._cobj, move)
        except OSError as e:
            print("Unable to make move {}, cached legal moves were: {}".format(move, self.getLegalMoves()))
            raise e
        self.__legalMoves = None

    def getLegalMoves(self):
        if (self.__legalMoves):
            return list(self.__legalMoves)
        else:
            legalMoves = _bitStateDLL.getLegalMoves(self._cobj)
            # An expression like:
            # [m for m in Move.Move if ((legalMoves >> m.value) & 0b1) == 0b1]
            # is expensive, since m.value is expensive, so instead the conversion is hardcoded
            # using the static variable BitState.legalMoveBitPatternMap
            return list(BitState.legalMoveBitPatternMap[legalMoves])

    def isFinished(self):
        return _bitStateDLL.gameFinished(self._cobj)

    def getScore(self):
        return _bitStateDLL.getScore(self._cobj)

    def getTileValue(self, x, y):
        return _bitStateDLL.getTileValue(self._cobj, x, y)

    def getTileCount(self):
        return _bitStateDLL.getTileCount(self._cobj)

    def getUnsignedBinaryState(self):
        return _bitStateDLL.getUnsignedBinaryState(self._cobj)

    def getSignedBinaryState(self):
        # Transform the binary state from unsigned form to signed form to be used in tensorflow
        unsignedState = self.getUnsignedBinaryState()
        if (unsignedState & BitState.MSBSetMask):
            # If top bit is set, reinterpret as 2 complement. Substract 2**64 twice,
            # once for the value as it is currently set, once to reinterpret as negative value.
            return unsignedState - 2 * BitState.MSBSetMask
        else:
            return unsignedState
        


    # def clearTile(self, x, y):
    #     _bitStateDLL.clearTile(self._cobj, x, y)
    
    # def getTile(self, x, y):
    #     return _bitStateDLL.getTile(self._cobj, x, y)

    # def setTile(self, x, y, val):
    #     return _bitStateDLL.setTile(self._cobj, x, y, val)
