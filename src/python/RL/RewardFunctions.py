import os
import sys
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'../'))
from GameState import GameState
from Move import Move
class RewardFunctions():

    @staticmethod
    def simpleMergeReward(state : GameState, move : Move) -> float:
        """Simply return 1.0 if a merge was done (score increases), 0.0 otherwise

        Parameters
        ----------
        state : GameState : The current board state 
        move : Move : The move for which to determine the reward 

        Returns
        -------
        out : float : The reward value for the state move combination
        """
        score = state.getScore()
        state.makeMove(move)
        if (state.getScore() > score):
            return 1.0
        else:
            return 0.0