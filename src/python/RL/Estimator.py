import os
import sys
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'../'))
from typing import List
import tensorflow as tf
import numpy as np
import Move
class Estimator():
    """Class to represent the Neural Nets.
    Used for both the Q-network as well as the Target Network.
    """
    def __init__(self, scope : str, inputShape : tf.TensorShape):
        # Create the variables withing this scope, for easy retrieval later (for copying)
        self.scope : str = scope
        self.inputShape : tf.TensorShape = inputShape
        with tf.variable_scope(scope):
            self.__buildGraph()

    def __buildGraph(self) -> None:
        """Private method to build the control flow of our graph.
        """ 
        # Create placeholder variable to hold our x values, the board states in this case.
        # Its number of elements is unbounded (indicated by 'None'), and each state
        # has a shape described by self.inputShape
        xPlaceholderShape : tf.TensorShape = tf.TensorShape([None]).concatenate(self.inputShape)
        self.xPlaceholder = tf.placeholder(dtype=tf.int64, shape=xPlaceholderShape, name='x')
        # Same but for our y values, where the values are the 'targets'/'observations'?
        self.yPlaceholder = tf.placeholder(dtype=tf.float32, shape=[None], name='y')
        # Same but to represent the actions at every step
        self.actionsPlaceholder = tf.placeholder(dtype=tf.int32, shape=[None], name='actions')

        # Get the number of elements
        batchSize : tf.Tensor = tf.shape(self.xPlaceholder)[0]

        # Add 4 fully connected layers, the last holding the values of the predictions
        # ERROR: can't seem to create the layers using tf.uint8 as values, so map to float
        # Activation functions:
        # relu, derivative 1 for all inputs, instead of very low in sigmoid. This means it has a better error backpropagation
        # Softmax, choose just one, in this case a move, so last layer
        self.floatedX : tf.Tensor = tf.to_float(self.xPlaceholder)
        
        # # Reshape to contain 1 channel, channel is last element of shape, since data_format in tf.layers.conv2d is 'channels_last'
        # channeledXShape : List[int] = xPlaceholderShape.concatenate(tf.TensorShape([1])).as_list()
        
        # # Replace None value at the start by -1, since tf.reshape can handle unknowns if represented as -1, but not if represented as None
        # channeledXShape[0] = -1

        # self.channeledX = tf.reshape(self.floatedX, channeledXShape)
        # print(self.channeledX.get_shape())

        # Create the convolutional layers. 
        conv1 : tf.Tensor = tf.layers.conv2d(self.floatedX, 16, (2,2), activation=tf.nn.relu) #outputs 3 by 3 and then a relu function
        conv2 : tf.Tensor = tf.layers.conv2d(conv1, 16, (2,2), activation=tf.nn.relu) # outputs 2 by 2
        # Flatten the results from the conv4 convolutional layer to be used in a fully connected layer
        flatten : tf.Tensor = tf.layers.flatten(conv2)
        fc1 : tf.Tensor = tf.contrib.layers.fully_connected(flatten, 256, activation_fn=tf.nn.relu)
        self.predictions : tf.Tensor = tf.contrib.layers.fully_connected(fc1, len(Move.Move), activation_fn=tf.nn.softmax)

        # Get the predictions for the chosen actions only ?????????WHYYYYYY I DON'T UNDERSTAND??????
        gather_indices = tf.range(batchSize) * tf.shape(self.predictions)[1] + self.actionsPlaceholder
        # reshape with [-1] to flatten self.predictions and retrieve all the values at the indices in gather_indices
        self.action_predictions = tf.gather(tf.reshape(self.predictions, [-1]), gather_indices)

        # Calculate the loss by calculating the element wise squared difference
        # between the predictions and the actual observations.
        # self.losses = tf.squared_difference(self.yPlaceholder, self.action_predictions)
        # Compute the average of the losses along all axes, returns Tensor with 1 element
        # This is the Huber Loss (Temporal Difference)
        # self.loss = tf.reduce_mean(self.losses) / 2
        self.loss = tf.losses.huber_loss(self.yPlaceholder, self.action_predictions)

        # Optimiser based on http://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf
        # Arguments are: learning rate, decay, momentum, and epsilon
        self.optimizer = tf.train.RMSPropOptimizer(0.00025, 0.95, 0.01)
        # self.optimizer = tf.train.GradientDescentOptimizer(0.0001)
        self.trainOperation = self.optimizer.minimize(self.loss, global_step=tf.train.get_global_step())

        # Summaries for Tensorboard
        self.summaries = tf.summary.merge([
            tf.summary.scalar("loss", self.loss),
            tf.summary.histogram("q_values_hist", self.predictions),
            tf.summary.scalar("max_q_value", tf.reduce_max(self.predictions))
        ])

    def predict(self, session : tf.Session, observations : np.array):
        """
        Predicts action values.

        Parameters
        ----------
        session : tf.Session : Tensorflow session
        observations : np.array : Boardstate inputs, [batchSize, 16]

        Returns
        -------
        out : tf.Tensor : Tensor of shape [batchSize, NUM_VALID_ACTIONS] containing the estimated 
        action values.
        """
        feedDict = {self.xPlaceholder: observations}
        return session.run(self.predictions, feedDict)

    def update(self, session : tf.Session, states : tf.Tensor, actions : tf.Tensor, targets : tf.Tensor) -> tf.Tensor:
        """
        Updates the estimator towards the given targets.

        Parameters
        ----------
        session : tf.Session : Tensorflow session object
        states : tf.Tensor : State input of shape [batch_size, 16]
        actions : tf.Tensor : Chosen actions of shape [batch_size]
        targets : tf.Tensor : Targets of shape [batch_size]

        Returns
        -------
        out : tf.Tensor : The calculated loss on the batch as a Tensor of size 1.
        """
        # Perform the operations in the list in order: gather summaries, log the global step count,
        # do the training, compute the loss.
        feedDict = { self.xPlaceholder: states,  self.yPlaceholder: targets, self.actionsPlaceholder: actions }
        summaries, global_step, _, loss = session.run(
            [self.summaries, tf.train.get_global_step(), self.trainOperation, self.loss],
            feedDict)
        # Write the summary to file
        # if self.summary_writer:
        #     self.summary_writer.add_summary(summaries, global_step)

        # Return the loss
        return loss
