import os
import sys
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'../'))
from collections import deque
from typing import Tuple, List
from collections import namedtuple
from Move import Move
from ctypes import c_uint64, c_int8
from BitState import BitState
import random

# Define a named tuple to use a transition object
Transition = namedtuple('Transition', ['state', 'processedState', 'action', 'reward', 'nextState', 'gameFinished'], verbose=False)

class ReplayMemory():

    def __init__(self, maxSize : int) -> None:
        self.__queue = deque(maxlen=maxSize)

    def add(self, replayEntity : Transition) -> None:
        """Add element to replay memory.
        The deque class handles removing old items if there are more than maxlen items.

        Parameters
        ----------
        replayEntity : Transition : a tuple consisting of (state, action, reward, nextState, done)
        """
        self.__queue.append(replayEntity)
    
    def clear(self) -> None:
        """Clear the replay memory
        """  
        self.__queue.clear()

    def sample(self, sampleSize : int) -> List[Transition]:
        """Set docstring here.

        Parameters
        ----------
        sampleSize: the size of the sample 

        Returns
        -------
        out: list of sampleSize random elements of the replay memory
        """
        if (len(self.__queue) < sampleSize):
            raise ValueError("Trying to sample {} elements from a replay memory containing {} elements.".format(
                    sampleSize, len(self.__queue)
                )
            )
        return random.sample(self.__queue, sampleSize)

    def getMaxSize(self) -> int:
        return self.__queue.maxlen

    def __len__(self) -> int:
        return len(self.__queue)
    