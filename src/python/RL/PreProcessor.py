import os
import sys
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'../'))
import ctypes
import BitState
import tensorflow as tf

class Preprocessor():
    def __init__(self, bitStateFlag : bool = True):
        # Build the Tensorflow graph
        with tf.variable_scope("stateProcessor_plain"):
            if (bitStateFlag):
                self.input_state = tf.placeholder(shape=[], dtype=tf.int64)
            else:
                self.input_state = tf.placeholder(shape=[4,4], dtype=tf.int64)
            self.output : tf.Tensor = self.input_state

    @staticmethod
    def _splitBits(inputTensor : tf.Tensor, n : int, stepSize : int):
        return tf.map_fn(
            lambda x: tf.bitwise.bitwise_and(
                tf.to_int64(inputTensor // 2**(x * stepSize)),
                tf.constant(2**stepSize - 1, dtype=tf.int64) # Has to be of the same type as self.input_state
            ), 
            tf.range(n, dtype=tf.int64)
        )

    def getOutputShape(self) -> tf.TensorShape:
        """Return the shape of self.output to be used to determine the shapes of the tensors in the neural nets

        Returns
        -------
        out : tf.TensorShape : The shape of the self.output tensor
        """
        return tf.TensorShape(self.output.shape)

    def process(self, session, state):
        """
        Parameters
        ----------
        session: The Tensorflow Session to run the process in
        state: A bit representation of a 2048 board
        """
        return session.run(self.output, { self.input_state: state })

class PreprocessorBoard(Preprocessor):
    """Processes a board to a different representation. Splits in [4,4] board
    """
    def __init__(self, bitStateFlag : bool = True):
        with tf.variable_scope("stateProcessor_4_4_list"):
            if (bitStateFlag):
                self.input_state = tf.placeholder(shape=[], dtype=tf.int64)
                rowSplit : tf.Tensor = Preprocessor._splitBits(self.input_state, 16, 4)
                self.output : tf.Tensor = tf.reshape(rowSplit, [4, 4])
            else:
                self.input_state = tf.placeholder(shape=[4,4], dtype=tf.int64)
                self.output = self.input_state
            


class PreprocessorSplit(Preprocessor):
    """
    Processes a board to a different representation. Splits it into list of 16.
    """
    def __init__(self, bitStateFlag : bool = True):
        # Build the Tensorflow graph
        with tf.variable_scope("stateProcessor_16_list"):
            if (bitStateFlag):
                self.input_state = tf.placeholder(shape=[], dtype=tf.int64)
                self.output : tf.Tensor = Preprocessor._splitBits(self.input_state, 4, 16)
            else:
                self.input_state = tf.placeholder(shape=[4,4], dtype=tf.int64)
                self.output : tf.Tensor = tf.reshape(self.input_state, [-1])

class PreprocessorBinaryChannels(Preprocessor):
    """
    Processes a board to a different representation. Splits it into 16 4 by 4 channels, i.e.
    [4,4,16] with binary values indicating whether that position on the board contains the 2^i value where i
    is the index in the 16 channels. This allows to represent empty on channel 0 and up to 2^15=32768
    which is plenty
    """
    def __init__(self, bitStateFlag : bool = True):
        with tf.variable_scope("stateProcessor_16_binary_channels"):
            # Get input
            if (bitStateFlag):
                self.input_state = tf.placeholder(shape=[], dtype=tf.int64)
                # Split 64 bits into [4, 4] board
                intermediateShape1 : tf.Tensor = Preprocessor._splitBits(self.input_state, 16, 4)
                finalInputShape : tf.Tensor = tf.reshape(intermediateShape1, [4, 4])
            else:
                self.input_state = tf.placeholder(shape=[4,4], dtype=tf.int64)
                finalInputShape = self.input_state
            
            self.output : tf.Tensor = tf.map_fn(
                lambda x: tf.map_fn(
                    lambda y: tf.map_fn(
                        lambda i: tf.cond(tf.equal(finalInputShape[x][y], i), lambda: tf.constant(1, dtype=tf.int64), lambda: tf.constant(0, dtype=tf.int64)),
                        tf.range(16, dtype=tf.int64)
                    ),
                    tf.range(4, dtype=tf.int64)
                ),
                tf.range(4, dtype=tf.int64)
            )