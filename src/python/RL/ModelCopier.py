import Estimator
import tensorflow as tf
class ModelCopier():
    """
    Copy the values of one neural net to another.
    """
    def __init__(self, model1 : Estimator, model2 : Estimator):
        """Prepare the operations for copying

        Parameters
        ----------
        model1 : Estimator: Copy origin model 
        model2 : Estimator: Copy target model
        """
        # Gather the variables into a list
        model1Parameters = list(filter(lambda x: x.name.startswith(model1.scope), tf.trainable_variables()))
        model2Parameters = list(filter(lambda x: x.name.startswith(model2.scope), tf.trainable_variables()))

        # Sort the variables based on name
        model1Parameters = sorted(model1Parameters, key=lambda v: v.name)
        model2Parameters = sorted(model2Parameters, key=lambda v: v.name)

        # For each pair of variables, copy over the values from one value to another
        # And store that operation into a list
        self.updateOperations = []
        for (model1Var, model2Var) in zip(model1Parameters, model2Parameters):
            # Use 'assign' operator to update the ref with the new value
            operation = model2Var.assign(model1Var)
            self.updateOperations.append(operation)
    
    def execute(self, session: tf.Session) -> None:
        """Execute the copy operations the the provided session.

        Parameters
        ----------
        session : tf.Session : The session in which to perform the operations
        """
        # Run the operations
        session.run(self.updateOperations)
