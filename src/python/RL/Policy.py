import os
import sys
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'../'))
import Estimator
import tensorflow as tf
import numpy as np
from typing import List
from Move import Move

class Policy():

    def __init__(self, session : tf.Session, qEstimator : Estimator):
        """Create policy object saving the session and qEstimator

        Parameters
        ----------
        session : tf.Session : The session this policy should run in 
        qEstimator : Estimator : The Estimator object for the q-values 
        """
        self.session = session
        self.qEstimator = qEstimator
    
    def EpsilonGreedy(
        self,
        legalMoves : List[Move],
        observation : np.array,
        epsilon : float 
        ) -> np.array:
        """Implement the epsilon greedy algorithm on the arguments provided.

        Parameters
        ----------
        numberOfLegalActions : int: Number of legal actions
        observation : np.array: The current preprocessed state
        epsilon : float: The epsilon value to use 
        
        Returns
        -------
        out : np.array : Array of size [NUMBER_OF_VALID_ACTIONS] containing the probabilities these actions are chosen
        """
        # 
        NUMBER_OF_VALID_ACTIONS = len(Move)

        # Initialize an array containing NUMBER_OF_VALID_ACTIONS values each equal to epsilon / number of legal moves.
        # It holds the probabilities of each move being played. This now only holds the random part, summing to epsilon.
        # After the illegal move probabilities are set to 0.
        probs = np.ones(NUMBER_OF_VALID_ACTIONS, dtype=float) * epsilon / len(legalMoves)

        # Compute the best move according to the qEstimator and increase the value of that move in the array
        # Compute it for a single observation of [16], hence the expand to make it a 2D array of [1,16].
        # This means the result is also at index 0, since the batchSize is 1. Results in an array of size [numberOfActions].
        qValues = self.qEstimator.predict(self.session, np.expand_dims(observation, 0))[0]

        # Set the illegal moves probabilities to 0, qValues to 0
        for m in Move:
            if not m in legalMoves:
                v = m.value
                probs[v] = 0
                qValues[v] = np.finfo(float).min

        # Extract the best action index from the array of remaining qValues
        bestActionIndex = np.argmax(qValues)
        
        # Update probabilities to reflect that move bestActionIndex should be played if the move is non random
        probs[bestActionIndex] += (1.0 - epsilon)
        return probs