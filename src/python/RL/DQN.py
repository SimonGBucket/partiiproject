import os
import sys
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'../'))
import tensorflow as tf
import numpy as np
import random
from typing import Callable, Tuple
import itertools
from Estimator import Estimator
from PreProcessor import Preprocessor, PreprocessorBoard, PreprocessorBinaryChannels
from RewardFunctions import RewardFunctions
from ReplayMemory import ReplayMemory, Transition
from ModelCopier import ModelCopier
from Policy import Policy
from GameState import GameState
from Strategy import StrategyRandom
from Move import Move
from BitState import BitState

class DQN():
    """A class to implement the DQN algorithm as described: https://www.cs.toronto.edu/~vmnih/docs/dqn.pdf
    """
    def __init__(
        self,
        session : tf.Session,
        qEstimator : Estimator,
        targetEstimator : Estimator,
        stateProcessor : Preprocessor = Preprocessor(),
        rewardFunction : Callable[[GameState, Move], float] = RewardFunctions.simpleMergeReward, 
        numberOfEpisodes : int = 10,
        replayMemorySize : int = 100000,
        replayMemoryInitSize : int = 5000,
        targetUpdateBatchSize : int = 1000,
        discountFactor : float = 0.7,
        epsilonInit : float = 1.0,
        epsilonMinimum : float = 0.1,
        epsilonDecaySteps : int = 100000,
        batchSize : int = 32,
        checkPointDir : str = "./checkpoints/"):
        """
        Parameters
        ----------
        session : tf.Session : The session the program runs in
        qEstimator : Estimator : Estimator object for the q values
        targetEstimator : Estimator : Estimator object for the targets
        stateProcessor : Preprocessor : Preprocessor object (or subclass) which processes the data to a different format
        numberOfEpisodes : int : the number of episodes, where an episode is a playthrough until a terminal state is reached
        replayMemorySize : int : number of transitions to hold in replay memory before starting to pop them
        targetUpdateBatchSize : int : The number of transitions after which to update the target network
        discountFactor : float : The discount factor for rewards further from current time
        epsilonInit : float : The initial epsilon value
        epsilonMinimum : float : The minimum epsilon value
        epsilonDecaySteps : float : Number of steps in which to decay from epsilonInit to epsilonMinimum
        batchSize : int : Number of elements to sample from the replay memory
        checkPointDir : str : directory to save the checkpoints in
        """
        self.session : tf.Session = session
        self.qEstimator : Estimator = qEstimator
        self.targetEstimator : Estimator = targetEstimator
        self.stateProcessor : Preprocessor = stateProcessor
        self.rewardFunction : Callable([GameState, Move], float) = rewardFunction
        self.numberOfEpisodes : int = numberOfEpisodes
        # Create a replay memory with maximum size as specified
        self.replayMemory : ReplayMemory = ReplayMemory(replayMemorySize)
        self.targetUpdateBatchSize : int = targetUpdateBatchSize
        self.discountFactor : float = discountFactor
        # Calculate the replay values, exponential decrease between epsilonInit and epsilonMinimum (inclusive)
        self.epsilonValues : np.array = np.linspace(
            start=epsilonInit,
            stop=epsilonMinimum,
            num=epsilonDecaySteps,
            endpoint=True
        )
        # self.epsilonValues : np.array = np.logspace(
        #     start=np.log(epsilonInit),
        #     stop=np.log(epsilonMinimum),
        #     num=epsilonDecaySteps,
        #     endpoint=True,
        #     base=np.e
        # )
        self.batchSize : int = batchSize
        self.globalTime : tf.Tensor = self.session.run(tf.train.get_global_step())

        # Create the ModelCopier
        self.modelCopier = ModelCopier(self.qEstimator, self.targetEstimator)

        # Retrieve possible saved checkpoints
        self.checkpointPath = os.path.join(os.path.abspath(os.path.dirname(__file__)),checkPointDir)
        if not os.path.exists(self.checkpointPath):
            os.makedirs(self.checkpointPath)
        self.saver = tf.train.Saver()
        self.__loadSavedCheckpoint()

        # Create the policy object
        self.policy = Policy(self.session, self.qEstimator)

        # Only populate the replay memory with some states if you need them, so check at start of run
        self.replayMemoryInitSize = replayMemoryInitSize

        self.gameScoreHistory = []
        

    def __loadSavedCheckpoint(self):
        """Try and load a checkpoint from self.checkpointPath
        """
        latestCheckpoint = tf.train.latest_checkpoint(self.checkpointPath)
        if latestCheckpoint:
            print("Restoring checkpoint from '{}'".format(self.checkpointPath))
            self.saver.restore(self.session, latestCheckpoint)

    def __populateReplayMemory(self):
        """Initial population of the replay memory with transitions until full.
        """
        print("Initialising replay memory with {} more elements.".format(self.replayMemoryInitSize - len(self.replayMemory)))
        state = GameState()
        for i in range(self.replayMemoryInitSize):
            preProcessedState = self.stateProcessor.process(self.session, state.state)
            actionChosen = StrategyRandom.getNextMove(state)
            nextState = state.copy()
            reward = self.rewardFunction(nextState, actionChosen)
            # Add the transition to replay memory
            transition = Transition(
                state,
                preProcessedState,
                actionChosen,
                reward,
                nextState,
                nextState.isFinished()
            )
            self.replayMemory.add(transition)
            # Progress to the next state. If the game is finished create a new game.
            # Otherwise, the new state can be extracted from transition
            if (nextState.isFinished()):
                state = GameState()
            else:
                state = nextState.copy()
            

    def __performStep(self, state : GameState) -> Transition:
        """Helper function to perform a single step. Returns a tuple containing the Transition
        Parameters
        -------
        state : GameState : The state to perform the step in

        Returns
        -------
        out : Tuple(GameState, Move, float, GameState, bool) : The transition resulting from the step
        """
        # Preprocess the state in binary using the give preprocessor
        preProcessedState = self.stateProcessor.process(self.session, state.state)

        # Get the probabilities for each action, using the correct epsilon value
        legalMoves = state.getLegalMoves()
        actionProbabilities = self.policy.EpsilonGreedy(
            legalMoves=legalMoves,
            observation=preProcessedState,
            epsilon=self.epsilonValues[min(self.globalTime, len(self.epsilonValues) - 1)])

        # Choose an action
        actionIndex = np.random.choice(np.arange(0, len(Move)), size=None, replace=False, p=actionProbabilities)
        actionChosen = Move(actionIndex)
        
        # Copy for nextState
        nextState : GameState = state.copy()

        # Get the reward of the move, also performs the action on nextState
        reward = self.rewardFunction(nextState, actionChosen)

        # Return the transition tuple
        return Transition(
            state,
            preProcessedState,
            actionChosen,
            reward,
            nextState,
            nextState.isFinished()
        )

    def run(self):
        # Check if replay memory need to be populated
        if (len(self.replayMemory) < self.replayMemoryInitSize):
            self.__populateReplayMemory()

        for episodeCount in range(1, self.numberOfEpisodes + 1):
            # Save to a checkpoint
            self.saver.save(self.session, self.checkpointPath)

            # Initialise variables used in playthrough
            state = GameState()
            loss : float = None

            # While loop until break. t keeps track of number of cycles.
            for t in itertools.count():
                # Check if target Estimator needs to be updated
                if (self.globalTime % self.targetUpdateBatchSize == 0):
                    self.modelCopier.execute(self.session)
                    print("Episode {}, Step {}, Copied the qEstimator to the target-Estimator".format(episodeCount, t))
                
                # Perform a single step
                transition = self.__performStep(state)

                # Add it to the replay memory
                self.replayMemory.add(transition)
                
                # Print some stats for keeping track of progress
                print("{} / {}, {} (/ {}), epsilon: {}, state: {}, score: {}, loss: {}".format(
                    episodeCount,
                    self.numberOfEpisodes,
                    t,
                    self.globalTime,
                    self.epsilonValues[min(self.globalTime, len(self.epsilonValues) - 1)],
                    state.state,
                    state.getScore(),
                    loss)
                )
            

                # Sample self.batchSize elements from the replay memory for training the qEstimator
                samples = self.replayMemory.sample(self.batchSize)
                # Split those elements into separate arrays by element, using * operator in zip to unzip
                _, processedStateArray, actionArray, rewardArray, nextStateArray, gameFinishedArray = map(np.array, zip(*samples))
                                
                # Calculate the qValues of the next states
                qValuesNextState = self.targetEstimator.predict(self.session, processedStateArray)

                # Use the values to calculate the target values.
                # Add the rewards to the discounted qValues of the next possible states if it is not finished.
                # Use np.invert to convert bool to (0, 1) but inverted.
                # np.amax gets the maximum value along the 2nd axis, so it returns an array containing all the maximum move values for all the states. 
                targetValues = rewardArray + np.invert(gameFinishedArray).astype(np.float32) * self.discountFactor * np.amax(qValuesNextState, axis=1)

                # Using the values computed, update the qEstimator
                loss = self.qEstimator.update(self.session, processedStateArray, actionArray, targetValues)

                # If game finished, break and start next episode
                if (transition.gameFinished):
                    self.gameScoreHistory.append(transition.nextState.score)
                    break

                # Else update state and globalStep
                state = transition.nextState.copy()
                self.globalTime += 1

    def getAveragePlayout(self) -> Tuple[int, int]:
        """Helper function to determine the current performance of the neural net

        Parameters
        ----------
        numberOfGames : int : The number of games to play to determine the statistics 

        Returns
        -------
        out : Tuple[int, int] : (number of games, total score)
        """
        averageScore = sum(self.gameScoreHistory)/len(self.gameScoreHistory)
        self.gameScoreHistory = [] 
        return averageScore

    def getMove(self, state : BitState):
        preProcessed = self.stateProcessor.process(self.session, state.getSignedBinaryState())
        qValues = self.qEstimator.predict(self.session, np.expand_dims(preProcessed, 0))[0]
        # Set Qvalues for illegal moves to 0
        for m in Move:
            if not m in state.getLegalMoves():
                qValues[m.value] = np.finfo(float).min
        return np.argmax(qValues)

def setupDQNEnvironment(bitStateFlag : bool = False) -> DQN:
    numberOfEpisodes = 5
    globalStep = tf.Variable(0, name='global_step', trainable=False)
    preProcessor = PreprocessorBinaryChannels(bitStateFlag)
    qEstimator = Estimator('qEstimator', preProcessor.getOutputShape())
    targetEstimator = Estimator('targetEstimator', preProcessor.getOutputShape())
    session = tf.Session()
    session.run(tf.global_variables_initializer())
    a : DQN = DQN(session, qEstimator, targetEstimator, preProcessor)
    return a

def main():
    a = setupDQNEnvironment()
    while True:
        a.run()
        sessionCount += numberOfEpisodes
        statisticsHistory.append((sessionCount, a.getAveragePlayout()))
        print(statisticsHistory)
        with open("RLData.txt", "w") as f:
            f.write(str(statisticsHistory))
if __name__ == "__main__":
    main()
            
