class MutableTree():
    def __init__(self, value, parent = None, children = None):
        self.__value = value
        self.__parent = parent
        if (children):
            self.__children = list(children)
        else:
            self.__children = []

    def isLeafNode(self):
        return self.__children == []

    def isRootNode(self):
        return self.__parent == None
    
    def getChildren(self):
        return self.__children

    def getParent(self):
        return self.__parent

    def getValue(self):
        return self.__value

    def setValue(self, val):
        self.__value = val

    def addChild(self, node):
        self.__children.append(node)

    def getChildByPredicate(self, pred):
        return list(filter(pred, self.__children))