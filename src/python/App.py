import os
import sys
from appJar import gui
import math
from enum import Enum
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)),'./RL/'))
import Strategy
from GameState import GameState
from BitState import BitState
from MonteCarloWrapper import MonteCarloWrapper
from Move import Move
import DQN
class GUIStrategy(Enum):
    MANUAL = 1
    UCT = 2
    DQN = 3

    @staticmethod
    def UCTStrategy(gameState : BitState) -> None:
        uct = MonteCarloWrapper.fromState(gameState)
        uct.runCycle(100)
        gameState.makeMove(uct.getMaxAverageScoreChild())

class App(gui):
    playboardSize = 4
    def __init__(self, title="2048 Emulator", size="400x400"):
        gui.__init__(self, title, size)
        for x in range(App.playboardSize):
            for y in range(App.playboardSize):
                self.addLabel(App._getLabelName(x, y), "", y, x)
                self.setLabelWidth(App._getLabelName(x, y), 400 // App.playboardSize)
                self.setLabelHeight(App._getLabelName(x, y), 400 // App.playboardSize)

    def _getLabelName(x, y):
        return str(x) + str(y)

    def getLabelByCoords(self, x, y):
        return self.getLabel(App._getLabelName(x, y))

    def setLabelByCoords(self, x, y, text):
        self.setLabel(App._getLabelName(x, y), text)

class Emulator(App):
    
    def __init__(self, strategy : GUIStrategy = GUIStrategy.MANUAL):
        App.__init__(self)
        self.__gameState = BitState()
        self.__strategyObject = None
        self.__callback = None
        if (strategy == GUIStrategy.MANUAL):
            this = self
            self.__callback = self._keyPressed
            for c in "asdw":
                self.bindKey(c, self.__callback)
        else:
            this = self
            self.bindKey("<space>", lambda _: this._runLoop())
            if (strategy == GUIStrategy.UCT):
                self.__callback = GUIStrategy.UCTStrategy
            elif (strategy == GUIStrategy.DQN):
                self.__strategyObject = DQN.setupDQNEnvironment(True)
                self.__callback = lambda x: this.__gameState.makeMove(this.__strategyObject.getMove(x))
        self._update()
        self.go()
    
    def _runLoop(self):
        self.__callback(self.__gameState)
        self._update()
        if (self.__gameState.isFinished()):
            self.restartQuestion(self.yesNoBox("Game Over", "Game over. Do you want to restart?"))
    
    def _keyPressed(self, key):
        moveMade = {
            "a": Move.LEFT,
            "s": Move.DOWN,
            "w": Move.UP,
            "d": Move.RIGHT
        }[key]
        if (not(moveMade in self.__gameState.getLegalMoves())):
            return
        self.__gameState.makeMove(moveMade)
        self._update()
        if (self.__gameState.isFinished()):
            self.restartQuestion(self.yesNoBox("Game Over", "Game over. Do you want to restart?"))
    
    def restartQuestion(self, answer : bool):
        if (answer):
            self.__gameState = BitState()
            self._update()
        else:
            exit()

    def _update(self):
        def getColor(value):
            valueBackcolourMap = ["#eee4da", "#ede0c8", "#f2b179", "#f59563", "#f67c5f", "#f65e3b", "#edcf72", "#edcc61", "#edc850", "#edc53f"]
            valueForecolourMap = ["#776e65", "#776e65", "#f9f6f2", "#f9f6f2", "#f9f6f2", "#f9f6f2", "#f9f6f2", "#f9f6f2", "#f9f6f2", "#f9f6f2"]
            valueColorMap = ["#776E65", "#FFF8D7", "#FFED97", "#FFBB77", "#FF9224", "#FF5809", "#EA0000", "#FFFF37", "#F9F900", "#E1E100", "#C4C400"]
            if (value == ""):
                return (valueColorMap[0], valueForecolourMap[0])
            else:
                log2Value = int(math.log2(int(value)))
                foreColourMapIndex = min(
                    log2Value,
                    len(valueForecolourMap) - 1
                )
                colorIndex = min(
                    log2Value,
                    len(valueColorMap) - 1)
                return (valueColorMap[colorIndex], valueForecolourMap[foreColourMapIndex])

        self.setTitle("2048 - Score: {}".format(self.__gameState.getScore()))
        for x in range(4):
            for y in range(4):
                val = self.__gameState.getTileValue(x, y)
                newVal = str(val) if val else ""
                self.setLabelByCoords(x, y, newVal)
                labelName = App._getLabelName(x, y)
                colorCode = getColor(self.getLabelByCoords(x, y))
                self.setLabelBg(labelName, colorCode[0])
                self.setLabelFg(labelName, colorCode[1])

a = Emulator(strategy=GUIStrategy.DQN)