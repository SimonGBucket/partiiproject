import os.path
import ctypes
def loadDLL(dllName):
    def getCurDir():
        return os.path.dirname(os.path.abspath(__file__))
    
    def getParent(dir):
        return os.path.join(dir, "..")

    def getChild(dir, childName):
        return os.path.join(dir, childName)
    dllPath = getChild(getChild(getChild(getParent(getParent(getCurDir())), "out"), "dll"), dllName)
    print("LOADING DLL:", dllPath)
    return ctypes.cdll.LoadLibrary(dllPath)