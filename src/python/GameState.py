from Move import Move
import random
class GameState():
    def __init__(self, boardSize = 4, tile2Chance = 0.9):
        self._2TileChance = tile2Chance
        self.boardSize = boardSize
        self._gameFinised = False
        self._legalMoves = None
        self.state = [[0 for _ in range(4)] for _ in range(4)]
        self.score = 0
        self._spawnTile()
        self._spawnTile()

    def copy(self):
        g = GameState(self.boardSize, self._2TileChance)
        g._gameFinised = self._gameFinised
        g.state = list(map(list, self.state))
        g._legalMoves = list(self._legalMoves) if (self._legalMoves) else None
        g.score = self.score
        return g

    def isFinished(self):
        return self._gameFinised

    def getScore(self):
        return self.score

    def getTileValue(self, x, y):
        return self.state[x][y]

    def getTileCount(self):
        return sum([1 if (self[x][y]) else 0 for x in range(4) for y in range(4)])

    def makeMove(self, move):
        if (self._gameFinised):
            return
        if (move == Move.LEFT):
            self._moveHorizontal(-1)
        elif (move == Move.DOWN):
            self._moveVertical(1)
        elif (move == Move.UP):
            self._moveVertical(-1)
        elif (move == Move.RIGHT):
            self._moveHorizontal(1)
        self._spawnTile()
        self._legalMoves = None
        if (len(self.getLegalMoves()) == 0):
            self._gameFinised = True

    def getLegalMoves(self):        
        def isLegalMove(xVector, yVector, dx, dy):
            for x in xVector:
                for y in yVector:
                    if (self.state[x][y]):
                        borderValue = self.state[x + dx][y + dy]
                        if (borderValue == 0 or borderValue == self.state[x][y]):
                            return True
            return False

        
        if (self._legalMoves):
            return self._legalMoves
        self._legalMoves = []
        for dx, dy, xVector, yVector, move in [
            (-1, 0, range(1, self.boardSize), range(self.boardSize), Move.LEFT),
            (1, 0, range(self.boardSize - 1), range(self.boardSize), Move.RIGHT),
            (0, 1, range(self.boardSize), range(self.boardSize - 1), Move.DOWN),
            (0, -1, range(self.boardSize), range(1, self.boardSize), Move.UP)
        ]:
            if (isLegalMove(xVector, yVector, dx, dy)):
                self._legalMoves.append(move)
        return self._legalMoves

    def _spawnTile(self):
        openTiles = [(x, y)
                    for x in range(self.boardSize)
                    for y in range(self.boardSize)
                    if (not(self.state[x][y]))]
        if (openTiles == []):
            self._gameFinised = True
        else:
            pos = openTiles[int(random.random() * len(openTiles))]
            tileValue = 4 if (random.random() > self._2TileChance) else 2
            self.state[pos[0]][pos[1]] = tileValue
    
    def _moveHorizontal(self, dx):
        if (dx == 1):
            borderX = 3
            xRange = range(3, -1, -1)
        else:
            borderX = 0
            xRange = range(4)

        for y in range(4):
            lastOpenSquareX = borderX
            for x in xRange:
                curValue = self.state[x][y]
                if (curValue):
                    if (lastOpenSquareX != borderX
                     and self.state[lastOpenSquareX + dx][y] == curValue):
                        self.state[x][y] = 0
                        self.state[lastOpenSquareX + dx][y] = curValue * 2
                        self.score += curValue * 2
                    else:
                        self.state[x][y] = 0
                        self.state[lastOpenSquareX][y] = curValue
                        lastOpenSquareX -= dx

    def _moveVertical(self, dy):
        if (dy == 1):
            borderY = 3
            yRange = range(3, -1, -1)
        else:
            borderY = 0
            yRange = range(4)

        for x in range(4):
            lastOpenSquareY = borderY
            for y in yRange:
                curValue = self.state[x][y]
                if (curValue):
                    if (lastOpenSquareY != borderY 
                    and self.state[x][lastOpenSquareY + dy] == curValue):
                        self.state[x][y] = 0
                        self.state[x][lastOpenSquareY + dy] = curValue * 2
                        self.score += curValue * 2
                    else:
                        self.state[x][y] = 0
                        self.state[x][lastOpenSquareY] = curValue
                        lastOpenSquareY -= dy