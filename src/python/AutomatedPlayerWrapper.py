import BitState
import DLLLoader
import ctypes
_automatedPlayerDLL = DLLLoader.loadDLL("AutomatedPlayer.dll")

_automatedPlayerDLL.newRandomAutomatedPlayer.restype = ctypes.c_void_p
_automatedPlayerDLL.newRandomAutomatedPlayer.argtypes = [ctypes.c_void_p, ctypes.c_uint32]

_automatedPlayerDLL.destroyAutomatedPlayer.restype = None
_automatedPlayerDLL.destroyAutomatedPlayer.argtypes = [ctypes.c_void_p]

_automatedPlayerDLL.play.restype = None
_automatedPlayerDLL.play.argtypes = [ctypes.c_void_p]

_automatedPlayerDLL.getTotalScore.restype = ctypes.c_uint32
_automatedPlayerDLL.getTotalScore.argtypes = [ctypes.c_void_p]

_automatedPlayerDLL.getNumberOfMoves.restype = ctypes.c_uint32
_automatedPlayerDLL.getNumberOfMoves.argtypes = [ctypes.c_void_p]

class AutomatedPlayerWrapper():

    def RandomPlayer(state = BitState.BitState(), numberOfGames = 1):
        return AutomatedPlayerWrapper(
            _automatedPlayerDLL.newRandomAutomatedPlayer(state.getCObj(), numberOfGames)
        )

    def __init__(self, cobj):
        self.__cobj = cobj

    def __exit__(self, exc_type, exc_val, exc_tb):
        _automatedPlayerDLL.destroyAutomatedPlayer(self.__cobj)

    def play(self):
        _automatedPlayerDLL.play(self.__cobj)

    def getTotalScore(self):
        return _automatedPlayerDLL.getTotalScore(self.__cobj)

    def getNumberOfMoves(self):
        return _automatedPlayerDLL.getNumberOfMoves(self.__cobj)