#include "../headers/Strategy.hpp"

uint8_t Strategy::getRandomMove(uint8_t legalMoves) {
    static constexpr uint32_t hammingWeight[] = {
        0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4
    };
    // get the hamming weight of the 4 bit binary number, ie the number of legal moves 
    uint32_t numberOfLegalMoves = hammingWeight[legalMoves];
    // get the index of the random legal move
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::uniform_int_distribution<int> intDistribution(0, numberOfLegalMoves - 1);
    int randMoveIndex = intDistribution(gen);

    // for every 0 bit present at an index smaller or equal to the randIndex, increase move by 1
    // to select the correct move
    uint8_t move = randMoveIndex;
    for (int i = 0; i <= move; i++) {
        move += (legalMoves & 0b1) == 0b0;
        legalMoves >>= 1;
    }
    return move;
}

uint8_t Strategy::randomMoveStrategy(BitState& state) {
    // get the current legal moves
    uint8_t legalMoves = state.getLegalMoves();
    return Strategy::getRandomMove(legalMoves);
};

uint32_t Strategy::NUMBER_OF_CYCLES = 1000;

uint8_t Strategy::MCTSStrategy(BitState& state) {
    MonteCarlo m(state);
    m.runCycle(Strategy::NUMBER_OF_CYCLES);
    const MonteCarloTree * const bestChild = m.getMaxAverageScoreChild();
    // printf("%d,%f\n", bestChild->getMoveFromParent(), bestChild->getTotalScore() / (double) bestChild->getNumberOfPlays());
    return bestChild->getMoveFromParent();
}
