#include "../headers/AutomatedPlayer.hpp"
#include "../headers/MonteCarlo.hpp"
#include "../headers/BitState.hpp"
#include <string>
#include <fstream>
#include <chrono>

template<std::size_t> struct tuplePos{};

template <class T, size_t pos> std::ostream& printTupleToFile(const T& val, const char separator, std::ofstream& fs, tuplePos<pos>) {
    // Print value and separator to file stream
    fs << std::get<std::tuple_size<T>::value - pos>(val);
    fs << separator;

    // Do a recursive call
    return printTupleToFile(val, separator, fs, tuplePos<pos - 1>());
}

template <class T> std::ostream& printTupleToFile(const T& val, const char separator, std::ofstream& fs, tuplePos<1>) {
    return fs << std::get<std::tuple_size<T>::value - 1>(val);
}

template <class T> void writeResultsToCSV(const std::vector<T>& data, const char separator = ',', const std::string fileName = "results.txt") {
    std::ofstream fs(fileName);

    for (T value : data) {
        printTupleToFile(value, separator, fs, tuplePos<std::tuple_size<T>::value>());
        fs << std::endl;
    }
    fs.flush();
    fs.close();
}

std::vector<std::tuple<uint32_t, uint32_t, uint32_t> > varyNumberOfCycles(uint32_t start, uint32_t end, uint32_t increment, uint32_t numberOfTimes = 10) {
    std::vector<std::tuple<uint32_t, uint32_t, uint32_t> > results;
    for (uint32_t i = start; i <= end; i += increment) {
        Strategy::NUMBER_OF_CYCLES = i;
        for (uint32_t n = 0; n < numberOfTimes; n++) {
            AutomatedPlayer p(BitState(), &Strategy::MCTSStrategy);
            auto beginTime = std::chrono::high_resolution_clock::now();
            p.play();
            auto endTime = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime-beginTime).count();
            results.push_back(
                std::make_tuple(i, p.getScore(), p.getNumberOfMoves())
            );
            printf("%u,%u,%u,%u,%lld\n", i, n, (uint32_t) p.getScore(), p.getNumberOfMoves(), duration);
            writeResultsToCSV(results,',',std::to_string(start) + '-' + std::to_string(end) + "CycleResults.txt");
        }
    }
    return results;
}

std::vector<std::tuple<double, uint32_t, uint32_t> > varyExplorationFactor(double start, double end, double increment, uint32_t numberOfTimes = 10, uint32_t numberOfCycles = 100) {
    Strategy::NUMBER_OF_CYCLES = numberOfCycles;
    std::vector<std::tuple<double, uint32_t, uint32_t> > results;
    for (double i = start; i <= end; i += increment) {
        MonteCarlo::setExplorationFactor(i);
        for (uint32_t n = 0; n < numberOfTimes; n++) {
            AutomatedPlayer p(BitState(), &Strategy::MCTSStrategy);
            auto beginTime = std::chrono::high_resolution_clock::now();
            p.play();
            auto endTime = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime-beginTime).count();
            results.push_back(
                std::make_tuple(i, p.getScore(), p.getNumberOfMoves())
            );
            printf("%f,%u,%u,%u,%lld\n", i, n, (uint32_t) p.getScore(), p.getNumberOfMoves(), duration);
            writeResultsToCSV(results,',', std::to_string(start) + '-' + std::to_string(end) + "FactorResults.txt");
        }
    }
    return results;
}

std::vector<std::tuple<uint32_t, uint32_t, uint32_t, uint32_t> > gatherHighestTileStatistics(uint32_t start, uint32_t end, uint32_t increment, uint32_t numberOfTimes = 10) {
    std::vector<std::tuple<uint32_t, uint32_t, uint32_t, uint32_t> > results;
    for (uint32_t i = start; i <= end; i += increment) {
        Strategy::NUMBER_OF_CYCLES = i;
        for (uint32_t n = 0; n < numberOfTimes; n++) {
            AutomatedPlayer p(BitState(), &Strategy::MCTSStrategy);
            auto beginTime = std::chrono::high_resolution_clock::now();
            p.play();
            auto endTime = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime-beginTime).count();
            uint32_t highestTileValue = *p.getHighestTileValues().begin();
            results.push_back(
                std::make_tuple(i, p.getScore(), p.getNumberOfMoves(), highestTileValue)
            );
            printf("%u,%u,%u,%u,%u,%lld\n", i, n, (uint32_t) p.getScore(), p.getNumberOfMoves(), highestTileValue, duration);
            writeResultsToCSV(results,',',std::to_string(start) + '-' + std::to_string(end) + "HighestTileResults.txt");
        }
    }
    return results;
}

int main(int argc, char* argv[]) {
    if (argc != 5) {
        printf("Expected 4 arguments, received %d. Terminating.", argc - 1);
        return 0;
    }
    double start = std::atof(argv[1]);
    double end = std::atof(argv[2]);
    double increment = std::atof(argv[3]);
    int numberOfTimes = std::stoi(argv[4]);
    printf("%f, %f, %f, %u\n", start, end, increment, numberOfTimes);
    auto results = gatherHighestTileStatistics(start, end, increment, numberOfTimes);
}