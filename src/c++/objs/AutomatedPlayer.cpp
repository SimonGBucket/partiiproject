#include "../headers/AutomatedPlayer.hpp"

inline void AutomatedPlayer::updateStats(uint32_t score, uint32_t moves, uint32_t highestTileValue) {
    this->totalScore += score;
    this->totalMoves += moves;
    this->highestTileValues.push_back(highestTileValue);
};

void AutomatedPlayer::playSingleGame(void) {
    BitState newState(*this->state);
    if (newState.gameFinished()) return;
    uint32_t curDepth = 0;
    do {
        auto nextMove = this->nextMoveStrategy(newState);
        newState.makeMove(nextMove);
        curDepth++;
    } while (curDepth != this->depth && !newState.gameFinished());
    this->updateStats(newState.getScore(), newState.getMoveCount(), newState.getHighestTileValue());
};

AutomatedPlayer::AutomatedPlayer(
    const BitState& state,
    uint8_t (*nextMoveStrategy)(BitState& state),
    uint32_t numberOfGames,
    uint32_t depth
) : highestTileValues() {
    this->state = new BitState(state);
    this->nextMoveStrategy = nextMoveStrategy;
    this->numberOfGames = numberOfGames;
    this->depth = depth;
    this->totalScore = 0;
    this->totalMoves = 0;
};
AutomatedPlayer::~AutomatedPlayer(void) {
    delete this->state;
};

uint64_t AutomatedPlayer::getScore(void) const {
    return this->totalScore;
};

uint32_t AutomatedPlayer::getNumberOfMoves(void) const {
    return this->totalMoves;
};

const std::vector<uint32_t> AutomatedPlayer::getHighestTileValues(void) const {
    return this->highestTileValues;
}

void AutomatedPlayer::play(void) {
    this->totalScore = 0;
    this->totalMoves = 0;
    for (uint32_t i = 0; i < this->numberOfGames; i++) {
        this->playSingleGame();
    };
};

DLLEXPORT AutomatedPlayer* newRandomAutomatedPlayer(const BitState * const state, uint32_t numberOfGames) {
    return new AutomatedPlayer(*state, &Strategy::randomMoveStrategy, numberOfGames);
};

DLLEXPORT void destroyAutomatedPlayer(AutomatedPlayer * const player) {
    delete player;
};

DLLEXPORT void play(AutomatedPlayer * const player) {
    player->play();
}

DLLEXPORT uint32_t getTotalScore(const AutomatedPlayer * const player) {
    return player->getScore();
}

DLLEXPORT uint32_t getNumberOfMoves(const AutomatedPlayer * const player) {
    return player->getNumberOfMoves();
}
