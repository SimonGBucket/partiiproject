#include "../headers/BitState.hpp"

int BitState::tileValueScoreMap(uint8_t value) {
    static constexpr uint32_t map[] = {
        0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096,
        8192, 16384, 32768, 65536, 131072
    };
    return map[value];
}

bool BitState::isLegalMove(
    int dx,
    int dy,
    int xStart,
    int xEnd, 
    int yStart,
    int yEnd) {
    for (int x = xStart; x < xEnd; x++) {
        for (int y = yStart; y < yEnd; y++) {
            int curValue = this->getTile(x, y);
            if (curValue > 0) {
                int borderValue = this->getTile(x + dx, y + dy);
                if (borderValue == 0 || borderValue == curValue) return true;
            }
        }
    }
    return false;
}
void BitState::spawnTile(void) {
    static std::default_random_engine gen(time(NULL));
    static std::uniform_real_distribution<double> realDis(0.0,1.0);
    uint8_t value = realDis(gen) > this->tile2Chance ? 2 : 1;
    
    uint8_t openTileCount = 0;
    for (int x = 0; x < this->boardSize; x++) {
        for (int y = 0; y < this->boardSize; y++) {
            if (this->getTile(x, y) == 0) {
                openTileCount++;
            }
        }
    }
    std::uniform_int_distribution<int> intDis(0, openTileCount - 1);
    uint32_t index = intDis(gen);

    for (int x = 0; x < this->boardSize; x++) {
        for (int y = 0; y < this->boardSize; y++) {
            if (this->getTile(x, y) == 0) {
                if (index > 0) index--;
                else {
                    if (this->getTile(x, y) != 0)
                        printf("SPAWNING ON OCCUPIED TILE"); 
                    this->setTile(x, y, value);
                    return;
                }
            }
        }
    }
};
void BitState::moveHorizontal(int dx) {
    char borderX;
    char startX, endX;
    if (dx == 1) {
        borderX = 3;
        startX = 3;
        endX = -1;
    } else {
        borderX = 0;
        startX = 0;
        endX = 4;
    }
    for (char y = 0; y < this->boardSize; y++) {
        char lastOpenSquareX = borderX;
        for (char x = startX; x != endX; x -= dx) {
            char curValue = this->getTile(x, y);
            if (curValue != 0) {
                if (lastOpenSquareX != borderX
                && this->getTile(lastOpenSquareX + dx, y) == curValue) {
                    this->clearTile(x, y);
                    this->setTile(lastOpenSquareX + dx, y, curValue + 1);
                    this->score += this->tileValueScoreMap(curValue + 1);
                } else {
                    this->clearTile(x, y);
                    this->setTile(lastOpenSquareX, y, curValue);
                    lastOpenSquareX -= dx;
                }
            }
        }
    }
};
void BitState::moveVertical(int dy) {
    char borderY;
    char startY, endY;
    if (dy == 1) {
        borderY = 3;
        startY = 3;
        endY = -1;
    } else {
        borderY = 0;
        startY = 0;
        endY = 4;
    }
    for (char x = 0; x < this->boardSize; x++) {
        char lastOpenSquareY = borderY;
        for (char y = startY; y != endY; y -= dy) {
            char curValue = this->getTile(x, y);
            if (curValue != 0) {
                if (lastOpenSquareY != borderY
                && this->getTile(x, lastOpenSquareY + dy) == curValue) {
                    this->clearTile(x, y);
                    this->setTile(x, lastOpenSquareY + dy, curValue + 1);
                    this->score += this->tileValueScoreMap(curValue + 1);
                } else {
                    this->clearTile(x, y);
                    this->setTile(x, lastOpenSquareY, curValue);
                    lastOpenSquareY -= dy;
                }
            }
        }
    }
};
void BitState::clearTile(uint8_t x, uint8_t y) {
    this->state &= ~((uint64_t) 0b1111 << (x * 4 + y * 16));
};
uint8_t BitState::getTile(uint8_t x, uint8_t y) const {
    return (this->state >> (x * 4 + y * 16)) & (uint64_t) 0b1111;
};
void BitState::setTile(uint8_t x, uint8_t y, uint8_t val) {
    this->clearTile(x, y);
    this->state |= ((uint64_t) val) << (x * 4 + y * 16);
};
BitState::BitState(uint8_t boardSize, float tile2Chance) {
    this->state = 0;
    this->boardSize = boardSize;
    this->tile2Chance = tile2Chance;
    this->legalMoves = 0b10000;
    this->score = 0;
    this->moveCount = 0;
    this->spawnTile();
    this->spawnTile();
};
BitState::BitState(const BitState& b) {
    this->state = b.state;
    this->boardSize = b.boardSize;
    this->tile2Chance = b.tile2Chance;
    this->legalMoves = b.legalMoves;
    this->score = b.score;
    this->moveCount = b.moveCount;
};
void BitState::makeMove(uint8_t move) {
    if ((this->getLegalMoves() & (1 << move)) == 0) {
        printf("PLAYING ILLEGAL MOVE");
    }
    switch (move) {
        case 0: this->moveHorizontal(-1); break; // Left
        case 1: this->moveVertical(-1); break; // Up
        case 2: this->moveHorizontal(1); break; // Right
        case 3: this->moveVertical(1); break; // Down
        default: throw std::invalid_argument("Illegal Argument: move not recognized");
    }
    this->spawnTile();
    this->legalMoves = 0b10000;
    this->moveCount++;
};
uint8_t BitState::getLegalMoves(void) {
    if (this->legalMoves != 0b10000) {
        return this->legalMoves;
    }
    this->legalMoves = 0;
    static std::tuple<int, int, int, int, int, int, int> cases[] = {
        std::make_tuple(-1, 0, 1, this->boardSize, 0, this->boardSize, 0),
        std::make_tuple(0, -1, 0, this->boardSize, 1, this->boardSize, 1),
        std::make_tuple(1, 0, 0, this->boardSize - 1, 0, this->boardSize, 2),
        std::make_tuple(0, 1, 0, this->boardSize, 0, this->boardSize - 1, 3),
    };
    for (auto m : cases) {
        if (this->isLegalMove(
            std::get<0>(m),
            std::get<1>(m),
            std::get<2>(m),
            std::get<3>(m),
            std::get<4>(m),
            std::get<5>(m)
        )) {
            this->legalMoves |= 0b1 << std::get<6>(m);
        }
    }
    return this->legalMoves;
}
bool BitState::gameFinished(void) {
    return this->getLegalMoves() == 0;
}
uint32_t BitState::getScore(void) {
    return this->score;
}
uint32_t BitState::getMoveCount(void) {
    return this->moveCount;
}
uint64_t BitState::getState(void){
    return this->state;
}
uint32_t BitState::getTileValue(uint8_t x, uint8_t y) const {
    return this->tileValueScoreMap(this->getTile(x, y));
}

uint32_t BitState::getHighestTileValue(void) const {
    uint32_t highestTileValue = 0;
    for (char x = 0; x < this->boardSize; x++) {
        for (char y = 0; y < this->boardSize; y++) {
            uint32_t curTileValue = this->getTile(x, y);
            if (curTileValue > highestTileValue) highestTileValue = curTileValue;
        }
    }
    return this->tileValueScoreMap(highestTileValue);
}


uint8_t BitState::getTileCount(void) {
    uint32_t count = 0;
    uint64_t stateLeft = this->state;
    for (int i = 0; i < 16; i++) {
        if ((stateLeft & 0b1111) > 0) count++;
        stateLeft >>= 4;
    }
    return count;
}

uint64_t BitState::getState(void) const {
    return this->state;
}

DLLEXPORT BitState* newBitState(uint8_t boardSize, float tile2Chance) {
    return new BitState(boardSize, tile2Chance);
};

DLLEXPORT BitState* copyBitState(BitState * p) {
    return new BitState(*p);
}

DLLEXPORT void destroyBitState(BitState* p) {
    delete p;
};

DLLEXPORT void makeMove(BitState* p, uint8_t move) {
    p->makeMove(move);
};

DLLEXPORT uint32_t getLegalMoves(BitState* p) {
    return p->getLegalMoves();
};

DLLEXPORT bool gameFinished(BitState* p) {
    return p->gameFinished();
}

DLLEXPORT uint32_t getScore(BitState* p) {
    return p->getScore();
}

DLLEXPORT uint32_t getTileValue(BitState* p, uint8_t x, uint8_t y) {
    return p->getTileValue(x, y);
}

DLLEXPORT uint32_t getTileCount(BitState* p) {
    return p->getTileCount();
}

DLLEXPORT uint64_t getUnsignedBinaryState(BitState* p) {
    return p->getState();
}
// DLLEXPORT void clearTile(BitState* p, uint8_t x, uint8_t y) {
//     p->clearTile(x, y);
// };

// DLLEXPORT uint8_t getTile(BitState* p, uint8_t x, uint8_t y) {
//     return p->getTile(x, y);
// };

// DLLEXPORT void setTile(BitState* p, uint8_t x, uint8_t y, uint8_t val) {
//     p->setTile(x, y, val);
// };