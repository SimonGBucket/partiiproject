#include "../headers/MonteCarlo.hpp"

MonteCarloTree::MonteCarloTree(void) : MonteCarloTree(nullptr, -1) {}

MonteCarloTree::MonteCarloTree(
    MonteCarloTree * const parent,
    const uint8_t moveFromParent
) : parent(parent), moveFromParent(moveFromParent) {
    this->finishedExploring = false;
    this->numberOfPlays = 0;
    this->totalScore = 0;
    this->children = std::vector<MonteCarloTree *>();
};

MonteCarloTree::~MonteCarloTree(void) {
    MonteCarloTree * const parent = this->getParent();
    if (parent) parent->removeChild(this);
    if (this->getParent())
    for (auto child : children) {
        delete child;
    }
}

MonteCarloTree * const MonteCarloTree::getParent(void) const {
    return this->parent;
}

const std::vector<MonteCarloTree *> * const MonteCarloTree::getChildren(void) const {
    return &this->children;
}

void MonteCarloTree::addChild(MonteCarloTree * const child) {
    this->children.push_back(child);
}

void MonteCarloTree::removeChild(MonteCarloTree * const child) {
    // Erase-remove idiom
    this->children.erase(
        std::remove(
            this->children.begin(),
            this->children.end(),
            child
        ),
        this->children.end()
    );
}

const uint8_t MonteCarloTree::getMoveFromParent(void) const {
    return this->moveFromParent;
}

const uint32_t MonteCarloTree::getNumberOfPlays(void) const {
    return this->numberOfPlays;
}

const uint64_t MonteCarloTree::getTotalScore(void) const {
    return this->totalScore;
}

void MonteCarloTree::updateNumberOfPlays(uint32_t increment) {
    this->numberOfPlays += increment;
}

void MonteCarloTree::updateTotalScore(uint32_t increment) {
    this->totalScore += increment;
}

bool MonteCarloTree::isLeafNode(void) const {
    return this->children.empty();
}

bool MonteCarloTree::isRootNode(void) const {
    return this->parent == nullptr;
}

void MonteCarloTree::setFinishedExploring(void) {
    this->finishedExploring = true;
    if (this->parent
        && this->parent->getChildren()->size() == 4 // Only sure way to set as finished if 4 moves explored
        && std::all_of(
            this->parent->getChildren()->begin(),
            this->parent->getChildren()->end(),
            [](MonteCarloTree * child){
                return child->isFinishedExploring();
            }
        )
    ) {
        this->parent->setFinishedExploring();
    }
}

bool MonteCarloTree::isFinishedExploring(void) const {
    return this->finishedExploring;
}

uint32_t MonteCarloTree::getTreeDepth(void) const {
    uint32_t maxDepth = 0;
    for (auto child : this->children) {
        uint32_t depth = child->getTreeDepth();
        if (depth > maxDepth) maxDepth = depth;
    }
    return maxDepth + 1;
}

uint32_t MonteCarloTree::getNodeCount(void) const {
    uint32_t count = 1;
    for (auto child : this->children)
        count += child->getNodeCount();
    return count;
}


MonteCarloTree * const MonteCarlo::UCTUpperBoundOptimalChild(const std::vector<MonteCarloTree *> children) {
    const uint8_t numberOfChildren = children.size();

    // Find the average scores
    double UCTScores[numberOfChildren];
    for(uint8_t i = 0; i < numberOfChildren; i++) {
        MonteCarloTree * const child = children[i];
        UCTScores[i] = child->getTotalScore() / (double) child->getNumberOfPlays();
    }
    // for (int i = 0; i < numberOfChildren; i++) 
    //     printf("%f ", UCTScores[i]);
    // printf("\n");

    // Normalise the average scores
    const double minVal = *std::min_element(UCTScores, UCTScores + numberOfChildren);
    const double maxVal = *std::max_element(UCTScores, UCTScores + numberOfChildren);
    for (uint8_t i = 0; i < numberOfChildren; i++) {
        UCTScores[i] = (UCTScores[i] - minVal) / (maxVal - minVal);
    }
    // for (int i = 0; i < numberOfChildren; i++) 
    //     printf("%f ", UCTScores[i]);
    // printf("\n");

    // Add the exploration factor
    for (uint8_t i = 0; i < numberOfChildren; i++) {
        MonteCarloTree * const child = children[i];
        uint32_t numberOfPlays = child->getNumberOfPlays();
        uint32_t parentPlays = child->getParent()->getNumberOfPlays();
        UCTScores[i] += MonteCarlo::EXPLORATION_FACTOR * std::sqrt(std::log(numberOfPlays) / (double) parentPlays);
    }
    // for (int i = 0; i < numberOfChildren; i++) 
    //     printf("%f ", UCTScores[i]);
    // printf("\n");

    // Find the index of the max value
    uint8_t optimalChildIndex = std::distance(
        UCTScores,
        std::max_element(
            UCTScores,
            UCTScores + numberOfChildren
        )
    );
    // printf("MAX VALUE %f\n", UCTScores[optimalChildIndex]);

    // Return the optimal child
    return children[optimalChildIndex];
}

uint8_t MonteCarlo::movesLeft(MonteCarloTree * const parent, BitState * const game) {
    uint8_t gameLegalMoves = game->getLegalMoves();
    // Remove all legal moves already played
    for (MonteCarloTree * const child : *parent->getChildren()) {
        gameLegalMoves &= ~(1 << child->getMoveFromParent());
    }
    return gameLegalMoves;
}

MonteCarloTree * const MonteCarlo::getOptimalChild(MonteCarloTree * const parent, BitState * const game) {
    // Filter out the already explored nodes
    std::vector<MonteCarloTree *> filteredChildren;
    for (MonteCarloTree * child : *parent->getChildren()) {
        if (!child->isFinishedExploring()) {
            filteredChildren.push_back(child);
        }
    }
    // Filter out moves not legal for the game
    uint8_t gameLegalMoves = game->getLegalMoves();
    // Erase-remove idiom
    filteredChildren.erase(
        std::remove_if(
            filteredChildren.begin(),
            filteredChildren.end(),
            [=](const MonteCarloTree * a) {
                return (gameLegalMoves & (1 << a->getMoveFromParent())) == 0;
            }
        ),
        filteredChildren.end()
    );
    // Check if empty, if so, return nullptr to signal no moves left
    if (filteredChildren.empty()) {
        return nullptr;
    }
    // Select the best move left over
    return MonteCarlo::UCTUpperBoundOptimalChild(filteredChildren);
}

MonteCarloTree * const MonteCarlo::select(MonteCarloTree * const root, BitState * const game) {
    if (root->isLeafNode()) return root;
    MonteCarloTree * optimalNode = root;
    while (!game->gameFinished() && !MonteCarlo::movesLeft(optimalNode, game) && !optimalNode->isLeafNode()) {
        optimalNode = MonteCarlo::getOptimalChild(optimalNode, game);
        // Catch potential nullptr due to filtered possible child nodes
        if (!optimalNode) {
            break;
        }
        game->makeMove(optimalNode->getMoveFromParent());
    }
    return optimalNode;
}

MonteCarloTree * const MonteCarlo::expand(MonteCarloTree * const parent, BitState * const game) {
    uint8_t movesLeft = MonteCarlo::movesLeft(parent, game);
    uint8_t move = Strategy::getRandomMove(movesLeft);
    game->makeMove(move);
    MonteCarloTree * const newNode = new MonteCarloTree(
        parent,
        move           
    );
    parent->addChild(newNode);
    return newNode;
}

const std::tuple<uint64_t, uint32_t, uint32_t> MonteCarlo::simulate(BitState * const game, uint32_t simulateRuns, uint32_t simulateDepth) {
    AutomatedPlayer player = AutomatedPlayer(
        *game,
        &Strategy::randomMoveStrategy,
        simulateRuns,
        simulateDepth
    );
    player.play();
    return std::make_tuple<uint64_t, uint32_t, uint32_t>(
        std::move(player.getScore()),
        std::move(player.getNumberOfMoves()),
        std::move(simulateRuns)
    );
}

void MonteCarlo::backPropagate(
    MonteCarloTree * const node,
    const std::tuple<uint64_t, uint32_t, uint32_t> results
) {
    node->updateTotalScore(std::get<0>(results));
    node->updateNumberOfPlays(std::get<2>(results));
    if (!node->isRootNode()) backPropagate(node->getParent(), results);
}
MonteCarlo::MonteCarlo(void) : root(new MonteCarloTree()), rootState() {}
MonteCarlo::MonteCarlo(const BitState& rootState) : root(new MonteCarloTree()), rootState(rootState) {}
MonteCarlo::~MonteCarlo(void) {
    delete root;
}

void MonteCarlo::setExplorationFactor(double explorationFactor) {
    MonteCarlo::EXPLORATION_FACTOR = explorationFactor;
}
double MonteCarlo::EXPLORATION_FACTOR = sqrt(2); // initialise value

void MonteCarlo::runCycle(uint32_t numberOfCycles) {
    for (uint32_t i = 0; i < numberOfCycles; i++) {
        BitState game(this->rootState);
        MonteCarloTree * const optimalLeaf = MonteCarlo::select(this->root, &game);
        if (game.gameFinished()) {
            // printf("Game finished after %d cycles, state: %" PRIu64 "\n", i + 1, game.getState());
            const std::tuple<uint64_t, uint32_t, uint32_t> results = MonteCarlo::simulate(&game);
            MonteCarlo::backPropagate(optimalLeaf, results);
            continue;
        }

        MonteCarloTree * const newLeaf = MonteCarlo::expand(optimalLeaf, &game);
        const std::tuple<uint64_t, uint32_t, uint32_t> results = MonteCarlo::simulate(&game);
        MonteCarlo::backPropagate(newLeaf, results);
    }
}

// playoutType MonteCarlo::getOptimalPlayout(void) const {
//     BitState * const game = new BitState();
//     MonteCarloTree * optimalNode = this->root;
//     playoutType playout;
//     playout.push_back(std::make_tuple(
//         optimalNode->getMoveFromParent(),
//         optimalNode->getTotalScore(),
//         optimalNode->getNumberOfPlays()
//     ));
//     while (!game->gameFinished() && !optimalNode->isLeafNode()) {
//         optimalNode = MonteCarlo::getOptimalChild(optimalNode, game);
//         playout.push_back(std::make_tuple(
//             optimalNode->getMoveFromParent(),
//             optimalNode->getTotalScore(),
//             optimalNode->getNumberOfPlays()
//         ));
//     }
//     delete game;
//     return playout;
// }

const MonteCarloTree * const MonteCarlo::getMaxAverageScoreChild(void) const {
    return *std::max_element(
        this->root->getChildren()->begin(),
        this->root->getChildren()->end(),
        [](MonteCarloTree * a, MonteCarloTree * b) {
            double aAverageScore = a->getTotalScore() / (double) a->getNumberOfPlays();
            double bAverageScore = b->getTotalScore() / (double) b->getNumberOfPlays();
            return aAverageScore < bAverageScore;
        }
    );
}

std::tuple<uint32_t, uint64_t, double, double> MonteCarlo::getAveragePlayoutScore(uint32_t numberOfGames) {
    uint64_t totalScore = 0;
    uint64_t totalNumberOfMoves = 0;
    BitState maxScoreGame;
    for (uint32_t i = 0; i < numberOfGames; i++) {
        BitState game;
        MonteCarlo::select(this->root, &game);
        if (game.getScore() > maxScoreGame.getScore()) {
            maxScoreGame = BitState(game);
        }
        const std::tuple<uint64_t, uint32_t, uint32_t> result = MonteCarlo::simulate(&game, 1, 0);
        // printf("%d: %d, %d, %d\n", i, game.getScore(), std::get<0>(result), std::get<1>(result));
        totalScore += std::get<0>(result);
        totalNumberOfMoves += std::get<1>(result);
    }
    printf("TOTAL NUMBER OF NODES %d\n", this->root->getNodeCount());
    return std::make_tuple<uint32_t, uint64_t, double, double>(this->root->getTreeDepth(), maxScoreGame.getState(), totalNumberOfMoves / (double) numberOfGames, totalScore / (double) numberOfGames);
}

DLLEXPORT MonteCarlo * newMonteCarlo(void) {
    return new MonteCarlo();
}

DLLEXPORT MonteCarlo * newMonteCarloFromState(BitState * state) {
    return new MonteCarlo(*state);
}

DLLEXPORT void deleteMonteCarlo(MonteCarlo * monteCarlo) {
    delete monteCarlo;
}

DLLEXPORT void runCycle(MonteCarlo * const monteCarlo, uint32_t numberOfCycles) {
    monteCarlo->runCycle(numberOfCycles);
}

DLLEXPORT uint8_t getMaxAverageScoreChild(MonteCarlo * monteCarlo) {
    return monteCarlo->getMaxAverageScoreChild()->getMoveFromParent();
}

// int main(int argc, char* argv[]) {
//     uint32_t numberOfCycles = atoi(argv[1]);
//     uint32_t numberOfStatsPlayouts = 10000;
//     MonteCarlo m;
//     m.runCycle(numberOfCycles);
//     // playoutType p = m.getOptimalPlayout();
//     // for ( const auto& i : p ) {
//     //     printf("%d, %" PRIu64", %d\n", std::get<0>(i), std::get<1>(i), std::get<2>(i));
//     // }
//     std::tuple<uint32_t, uint64_t, double, double> averagePlayoutStats = m.getAveragePlayoutScore(numberOfStatsPlayouts);
//     printf("Number of cycles:\t%d\nNumber of playouts:\t%d\nMax depth:\t\t%d\nBest board:\t\t%" PRIu64"\nAverage #moves:\t\t%f\nAverage score:\t\t%f\n",
//             numberOfCycles,
//             numberOfStatsPlayouts,
//             std::get<0>(averagePlayoutStats),
//             std::get<1>(averagePlayoutStats),
//             std::get<2>(averagePlayoutStats),
//             std::get<3>(averagePlayoutStats)
//         );
//     return 0;
// }

uint64_t bitStateToBoard(BitState * const state) {
    uint64_t result = 0;
    for (uint8_t x = 0; x < 4; x++) {
        for (uint8_t y = 0; y < 4; y++) {
            uint32_t tileValue = state->getTileValue(x, y);
            uint8_t count = 0;
            while (tileValue > 1) {
                tileValue >>= 1;
                ++count;
            }
            result = result * 10 + count;
        }
    }
    return result;
}