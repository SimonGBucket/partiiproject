#ifndef BIT_STATE_HPP
#define BIT_STATE_HPP 
#include <tuple>
#include <vector>
#include <random>
#include <stdexcept>
#include <cinttypes>
#include <stdio.h>
#include <time.h>       /* time */
#ifndef DLLEXPORT
#define DLLEXPORT extern "C" __declspec(dllexport)
#endif
class BitState final {
private:
    uint64_t state;
    float tile2Chance;
    uint8_t boardSize;
    uint32_t legalMoves;
    uint32_t moveCount;
    uint32_t score;

    static int tileValueScoreMap(uint8_t value);
    bool isLegalMove(
        int dx,
        int dy,
        int xStart,
        int xEnd, 
        int yStart,
        int yEnd);
    void spawnTile(void);
    void moveHorizontal(int dx);
    void moveVertical(int dy);
    void clearTile(uint8_t x, uint8_t y);
    uint8_t getTile(uint8_t x, uint8_t y) const;
    void setTile(uint8_t x, uint8_t y, uint8_t val);

public:
    BitState(uint8_t boardSize = 4, float tile2Chance = 0.9);
    BitState(const BitState& b);
    void makeMove(uint8_t move);
    uint8_t getLegalMoves(void);
    bool gameFinished(void);
    uint32_t getScore(void);
    uint32_t getMoveCount(void);
    uint64_t getState(void);
    uint32_t getTileValue(uint8_t x, uint8_t y) const;
    uint32_t getHighestTileValue(void) const;
    uint8_t getTileCount(void);
    uint64_t getState(void) const;
};
#endif