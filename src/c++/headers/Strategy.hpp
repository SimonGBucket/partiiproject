#ifndef STRATEGY_HPP
#define STRATEGY_HPP
#ifndef DLLEXPORT
#define DLLEXPORT extern "C" __declspec(dllexport)
#endif
#include <random>
#include <stdio.h>
#include "./BitState.hpp"
struct Strategy {
    static uint32_t NUMBER_OF_CYCLES; // default 1000
    static uint8_t getRandomMove(uint8_t legalMoves);
    static uint8_t randomMoveStrategy(BitState& state);
    static uint8_t MCTSStrategy(BitState& state);
};
#include "./MonteCarlo.hpp"
#endif