#ifndef MONTE_CARLO_HPP
#define MONTE_CARLO_HPP 
#include <stdio.h>
#include <tuple>
#include <cmath>
#include <algorithm>
#include <cinttypes>
#include <vector>
#include "./BitState.hpp"
#include "./AutomatedPlayer.hpp"
#include "./Strategy.hpp"
#ifndef DLLEXPORT
#define DLLEXPORT extern "C" __declspec(dllexport)
#endif

typedef std::vector<std::tuple<uint8_t, uint64_t, uint32_t> > playoutType;

class MonteCarloTree final {
private:
    bool finishedExploring;
    MonteCarloTree * const parent;
    const uint8_t moveFromParent;
    uint32_t numberOfPlays;
    uint64_t totalScore;
    std::vector<MonteCarloTree *> children;
public:
    MonteCarloTree(void);
    MonteCarloTree(
        MonteCarloTree * const parent,
        const uint8_t moveFromParent
    );
    ~MonteCarloTree(void);
    MonteCarloTree * const getParent(void) const;
    const std::vector<MonteCarloTree *> * const getChildren(void) const;
    void addChild(MonteCarloTree * const child);
    void removeChild(MonteCarloTree * const child);
    BitState * const getState(void);
    uint8_t getMovesLeft(void) const;
    void removeMove(uint8_t move);
    const uint8_t getMoveFromParent(void) const;
    const uint32_t getNumberOfPlays(void) const;
    const uint64_t getTotalScore(void) const;
    void updateNumberOfPlays(uint32_t increment);
    void updateTotalScore(uint32_t increment);
    bool isLeafNode(void) const;
    bool isRootNode(void) const;
    void setFinishedExploring(void);
    bool isFinishedExploring(void) const;
    uint32_t getTreeDepth(void) const;
    uint32_t getNodeCount(void) const;
};

class MonteCarlo final {
private:
    static const uint32_t SIMULATE_RUNS = 50;
    static const uint32_t SIMULATE_DEPTH = 0;
    static double EXPLORATION_FACTOR; // initialize to default value in MonteCarlo.cpp
    MonteCarloTree * const root;
    const BitState rootState;
    std::vector<uint8_t> optimalPlayout;
    static MonteCarloTree * const UCTUpperBoundOptimalChild(std::vector<MonteCarloTree *> children);
    static uint8_t movesLeft(MonteCarloTree * const parent, BitState * const game);
    static MonteCarloTree * const getOptimalChild(MonteCarloTree * const parent, BitState * const game);
    static MonteCarloTree * const select(MonteCarloTree * const root, BitState * const game);
    static MonteCarloTree * const expand(MonteCarloTree * const parent, BitState * const game);
    static const std::tuple<uint64_t, uint32_t, uint32_t> simulate(
        BitState * const game,
        uint32_t simulateRuns = MonteCarlo::SIMULATE_RUNS,
        uint32_t simulateDepth = MonteCarlo::SIMULATE_DEPTH
    );
    static void backPropagate(
        MonteCarloTree * const node,
        const std::tuple<uint64_t, uint32_t, uint32_t> results
    );
public:
    MonteCarlo(void);
    MonteCarlo(const BitState& rootState);
    ~MonteCarlo(void);
    static void setExplorationFactor(double explorationFactor = sqrt(2));
    void runCycle(uint32_t numberOfCycles);
    playoutType getOptimalPlayout(void) const;
    const MonteCarloTree * const getMaxAverageScoreChild(void) const;
    std::tuple<uint32_t, uint64_t, double, double> getAveragePlayoutScore(uint32_t numberOfGames = 100);
};

#endif