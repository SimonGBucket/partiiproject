#ifndef AUTOMATED_PLAYER_HPP
#define AUTOMATED_PLAYER_HPP 
#include <random>
#include <stdio.h>
#include <vector>
#include "./BitState.hpp"
#include "./Strategy.hpp"
#ifndef DLLEXPORT
#define DLLEXPORT extern "C" __declspec(dllexport)
#endif

class AutomatedPlayer final {
private:
    const BitState* state;
    uint8_t (*nextMoveStrategy)(BitState& state);
    uint32_t numberOfGames;
    uint64_t totalScore;
    uint32_t totalMoves;
    uint32_t depth;
    std::vector<uint32_t> highestTileValues;

    inline void updateStats(uint32_t score, uint32_t moves, uint32_t highestTileValue);
    void playSingleGame(void);
public:
    AutomatedPlayer(
        const BitState& state,
        uint8_t (*nextMoveStrategy)(BitState& state) = &Strategy::randomMoveStrategy,
        uint32_t numberOfGames = 1,
        uint32_t depth = 0
    );
    ~AutomatedPlayer(void);
    uint64_t getScore(void) const;
    uint32_t getNumberOfMoves(void) const;
    const std::vector<uint32_t> getHighestTileValues(void) const;
    void play(void);
};
#endif